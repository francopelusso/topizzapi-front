import React, { Component } from 'react';
import axios from 'axios';

class ErrorPage extends Component {
  state = { errorDescription: '' };

  render() {
    return (
      <div className="error">
        <span className="errorCode">{this.props.match.params.code}</span>
        <span className="errorDescription">{this.state.errorDescription}</span>
      </div>
    );
  }

  componentDidMount() {
    axios({
      method: 'get',
      url: `https://httpstat.us/${this.props.match.params.code}`,
      responseType: 'stream'
    })
      .then(response => {
        this.setState({errorDescription: response.data.description});
      })
      .catch(error => {
        this.setState({errorDescription: error.response.data.description});
      });
  }
}

export default ErrorPage;
