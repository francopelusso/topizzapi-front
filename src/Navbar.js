import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';

import './Navbar.css';

class Navbar extends Component {
  loginLink = (
    <div className="MenuItem">
      <a href="/login/">Login</a>
    </div>
  );
  registerLink = (
    <div className="MenuItem">
      <a href="/register/">Register</a>
    </div>
  );
  logoutLink = (
    <div className="MenuItem">
      <a href="/logout/">Logout</a>
    </div>
  );
  ordersLink = (
    <div className="MenuItem">
      <a href="/orders/">Orders</a>
    </div>
  );
  unauthenticated = (
    <div>
      {this.registerLink}
      {this.loginLink}
    </div>
  );
  authenticated = (
    <div>
      {this.logoutLink}
      {this.ordersLink}
    </div>
  );

  render() {
    const token = sessionStorage.getItem('TOKEN');
    const menu = token ? this.authenticated : this.unauthenticated;
    return (
      <header className="Navbar">
        <Row>
          <Col md="9" xs="12">
            <a href="/">
              <div className="LogoText">ToPizzApi</div>
            </a>
          </Col>
          <Col md="3" xs="12" className="Menu">
            {menu}
          </Col>
        </Row>
      </header>
    );
  }
}

export default Navbar;
