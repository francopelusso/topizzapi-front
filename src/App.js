import React from 'react';
import './App.css';
import { Container } from 'react-bootstrap';

import MainContainer from './MainContainer.js';
import Navbar from './Navbar.js';

function App() {
  return (
    <div className="AppContainer">
      <Navbar />
      <div className="App">
        <Container>
          <MainContainer />
        </Container>
      </div>
    </div>
  );
}

export default App;
