import axios from 'axios';
import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import './Login.css';

class LoginForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      success: false,
      errors: null,
      errorClass: null,
      token: sessionStorage.getItem('TOKEN'),
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    const params = this.props.location.search;
    this.next = params ? params.match(/next=(.*)/)[1] : '/';
  }

  handleChange(event) {
    this.setState({[event.target.name]: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();

    const fields = [this.state.username, this.state.password];
    if (fields.some(field => { return field === ''; })) {
      this.setState({
        errors: 'Please fill all fields.',
        errorClass: 'Errors'
      });

      return;
    }

    axios({
      method: 'post',
      'url': `${process.env.REACT_APP_API_URL}/oauth/token/`,
      data: {
        grant_type: 'password',
        username: this.state.username,
        password: this.state.password,
        client_id: 2,
        client_secret: process.env.REACT_APP_CLIENT_SECRET
      }
    })
      .then(response => {
        sessionStorage.setItem('TOKEN', response.data.access_token);
        sessionStorage.setItem('EXP', response.data.expires_in);
        sessionStorage.setItem('REFRESH_TOKEN', response.data.refresh_token);

        this.setState({success: true});

        setTimeout(() => {
          window.location.replace(this.next);
        }, 2000);
      })
      .catch(error => {
        const errorCode = error.response.status;
        window.location.replace(`/errors/${errorCode}`);
      });
  }

  render() {
    const toHome = (
      <p className="SmallText">
        <Link to='/'>Volver al inicio.</Link>
      </p>
    );

    if (!this.state.success && this.state.token) {
      return (
        <div>
          <h1 className="Errors">You are already logged in.</h1>
          {toHome}
        </div>
      )
    } else if (this.state.success) {
      return (
        <div>
          <h1 className="LoggedIn">You've been successfully logged in.</h1>
          {toHome}
        </div>
      );
    } else {
      return (
        <form onSubmit={this.handleSubmit}>
          <p className={this.state.errorClass}>{this.state.errors}</p>
          <Row className="LoginRow">
            <Col md="4" xs="12">
              <p>Email</p>
              <input type="text" name="username" onChange={this.handleChange} />
            </Col>
            <Col md="4" xs="12">
            <p>Password</p>
              <input type="password" name="password" onChange={this.handleChange} />
            </Col>
            <Col md="4" xs="12">
              <p>Submit</p>
              <input type="submit" value="Login" />
            </Col>
          </Row>
        </form>
      );
    }
  }
}

export default LoginForm;
