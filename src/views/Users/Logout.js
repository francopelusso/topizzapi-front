import React, { Component } from 'react';

import './Logout.css';

class Logout extends Component {
  constructor(props) {
    super(props);

    this.state = {success: false, token: sessionStorage.getItem('TOKEN')};
  }

  render() {
    if (!this.state.token && !this.state.success) {
      return <h1 className="Errors">You aren't logged in.</h1>;
    } else if (!this.state.success) {
      return <h1 className="Logout">Logging out.</h1>;
    } else {
      return <h1 className="Logout">You've been successfully logged out.</h1>;
    }
  }

  componentDidMount() {
    if (!this.state.token) return;

    sessionStorage.removeItem('TOKEN');
    sessionStorage.removeItem('REFRESH_TOKEN');
    sessionStorage.removeItem('EXP');

    this.setState({success: true, token: null});
  }
}

export default Logout;
