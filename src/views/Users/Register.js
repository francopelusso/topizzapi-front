import axios from 'axios';
import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: sessionStorage.getItem('RegisterName') || '',
      email: sessionStorage.getItem('RegisterEmail') || '',
      pass: sessionStorage.getItem('RegisterPass') || '',
      cPass: sessionStorage.getItem('RegisterCPass') || '',
      success: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const target = event.target;

    this.setState({ [target.name]: target.value });

    const storageName = target.name[0].toUpperCase() + target.name.substring(1);
    sessionStorage.setItem(`Register${storageName}`, target.value);

    if ((target.name === 'pass' && target.value === this.state.cPass) ||
      (target.name === 'cPass' && target.value === this.state.pass)
    ) {
      this.setState({errorClass: '', errors: ''});
    } else if (target.name === 'pass' || target.name === 'cPass') {
      this.setState({errorClass: 'Errors', errors: 'Passwords do not match.'});
    }
  }

  handleSubmit(event) {
    event.preventDefault();

    const fields = [this.state.name, this.state.email, this.state.pass];
    if (fields.some(field => { return field === ''; })) {
      this.setState({
        errors: 'Please fill all fields.',
        errorClass: 'Errors'
      });

      return;
    }

    if (this.state.pass !== this.state.cPass) {
      this.setState({
        errors: 'Passwords do not match.',
        errorClass: 'Errors'
      });

      return;
    }

    axios({
      method: 'post',
      'url': `${process.env.REACT_APP_API_URL}/register/`,
      data: {
        name: this.state.name,
        email: this.state.email,
        password: this.state.pass,
      }
    })
      .then(response => {
        sessionStorage.setItem('TOKEN', response.data.accessToken);
        sessionStorage.setItem('EXP', response.data.expiresAt);

        for (const field of ['Name', 'Email', 'Pass', 'CPass']) {
          sessionStorage.removeItem(`Register${field}`);
        }

        this.setState({success: true});

        setTimeout(() => {
          window.location.replace('/');
        }, 2000);
      })
      .catch(error => {
        console.log(error.response);
        if (error.response.data.message.indexOf(
          'UNIQUE constraint failed: users.email') !== -1
        ) {
          this.setState({
            errorClass: 'Errors',
            errors: 'Email already registered.'
          });
          return;
        }
        

        const errorCode = error.response.status;
        window.location.replace(`
          $/errors/${errorCode}
        `);
      });
  }

  render() {
    const token = sessionStorage.getItem('TOKEN');
    const toHome = (
      <p className="SmallText">
        <Link to="/">Volver al inicio.</Link>
      </p>
    );

    if (!this.state.success && token) {
      return (
        <div>
          <h1 className="Errors">You are registered and logged in.</h1>
          {toHome}
        </div>
      )
    } else if (this.state.success) {
      return (
        <div>
          <h1 className="Registered">You've registered successfully.</h1>
          {toHome}
        </div>
      );
    } else {
      return (
        <form onSubmit={this.handleSubmit}>
          <p className={this.state.errorClass}>{this.state.errors}</p>
          <Row className="LoginRow">
            <Col md="3" xs="12">
              <p>Name</p>
              <input
                type="text"
                name="name"
                onChange={this.handleChange}
                value={this.state.name}
              />
            </Col>
            <Col md="3" xs="12">
              <p>Email</p>
              <input
                type="email"
                name="email"
                onChange={this.handleChange}
                value={this.state.email}
              />
            </Col>
            <Col md="3" xs="12">
              <p>Password</p>
              <input
                type="password"
                name="pass"
                onChange={this.handleChange}
                value={this.state.pass}
              />

              <p>Confirm Password</p>
              <input
                type="password"
                name="cPass"
                onChange={this.handleChange}
                value={this.state.cPass}
              />
            </Col>
            <Col md="3" xs="12">
              <p>Submit</p>
              <input type="submit" value="Register" />
            </Col>
          </Row>
        </form>
      );
    }
  }
}

export default Register;
