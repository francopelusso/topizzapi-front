import axios from 'axios';
import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';

import Order from './Order';
import OrderForm from './OrderForm';
import OrdersItem from './OrdersItem';

import './index.css';

class Orders extends Component {
  state = {orders: []};

  render() {
    if (!sessionStorage.getItem('TOKEN')) {
      window.location.replace(`/login/?next=${this.props.location.pathname}`);
    }

    const ordersItems = this.state.orders.map(order => {
      return (
        <Col lg="4" md="6" sm="1" key={order.id}>
          <OrdersItem
            orderId={order.id}
            personName={order.person_name}
            orderedAt={order.created_at}
            price={order.price}
            phone={order.phone_number}
            address={order.address}
          />
        </Col>
      )
    });

    return (
      <div className="Orders">
        <h1>Last 15 Orders</h1>
        <div className="OrdersContainer">
          <Row className="OrdersRow">
            {ordersItems}
          </Row>
        </div>
      </div>
    )
  }

  componentDidMount() {
    axios({
      method: 'get',
      url: `${process.env.REACT_APP_API_URL}/orders/`,
      responseType: 'stream',
      'headers': {
        'Authorization': `Bearer ${sessionStorage.getItem('TOKEN')}`
      }
    })
      .then(response => {
        this.setState({ orders: response.data.data });
      })
      .catch(error => {
        const errorCode = error.response.status;
        window.location.replace(`/errors/${errorCode}`);
      });
  }
}

export { Orders, Order, OrderForm };
