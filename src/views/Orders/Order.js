import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faEuroSign, faCalendarDay, faHotel, faMobileAlt, faUser
} from '@fortawesome/free-solid-svg-icons';
import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';

import './Order.css';

class Order extends Component {
  state = { order: {pizzas: []} };

  render() {
    if (!sessionStorage.getItem('TOKEN')) {
      window.location.replace(`/login/?next=${this.props.location.pathname}`);
    }

    const orderedPizzas = this.state.order.pizzas.map(pizza => {
      return (
        <div className="OrderPizza" key={pizza.id}>
          <img
            className="OrderPizzaImage"
            src={pizza.image_url}
            alt={pizza.name}
          />
          <h5 className="OrderPizzaName">{pizza.name}</h5>
          <p className="OrderPizzaDescription">
            {pizza.description}
          </p>
          <p className="OrderPizzaPrice">
            Price:
            &nbsp;
            <FontAwesomeIcon icon={faEuroSign} />
            &nbsp;
            {pizza.price}
          </p>
        </div>
      );
    });

    return (
      <Row className="OrderRow">
        <Col md="6" xs="12" classname="Pizzas">
          <h3>Pizza</h3>
          {orderedPizzas}
        </Col>
        <Col md="6" xs="12">
          <h3>Details</h3>
          <Row>
            <Col md="6" xs="12">
              <p className="OrderName">
                <FontAwesomeIcon icon={faUser} />
                &nbsp;
                Ordered by: {this.state.order.person_name}
              </p>
            </Col>
            <Col md="6" xs="12">
              <p className="OrderPrice">
                Price:
                &nbsp;
                <FontAwesomeIcon icon={faEuroSign} />
                &nbsp;
                {this.state.order.price}
              </p>
            </Col>
            <Col md="6" xs="12">
              <p className="OrderAt">
                <FontAwesomeIcon icon={faCalendarDay} />
                &nbsp;
                At: {this.formatDate(this.state.order.created_at)}
              </p>
            </Col>
            <Col md="6" xs="12">
              <p className="OrderPhone">
                <FontAwesomeIcon icon={faMobileAlt} />
                &nbsp;
                Phone: {this.state.order.phone_number}
              </p>
            </Col>
            <Col xs="12">
              <p className="OrderAddress">
                <FontAwesomeIcon icon={faHotel} />
                &nbsp;
                Address: {this.state.order.address}
              </p>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }

  formatDate(date) {
    if (!date) return;

    const orderedAt = new Date(date);
    return `
      ${orderedAt.getDate()}.${orderedAt.getMonth()}.${orderedAt.getFullYear()} 
      ${orderedAt.getHours()}:${orderedAt.getMinutes()}:${orderedAt.getSeconds()}
    `;
  }

  componentDidMount() {
    axios({
      method: 'get',
      url: `
        ${process.env.REACT_APP_API_URL}/orders/${this.props.match.params.id}
      `,
      responseType: 'stream',
      headers: {
        'Authorization': `Bearer ${sessionStorage.getItem('TOKEN')}`
      }
    })
      .then(response => {
        this.setState({ order: response.data });
      })
      .catch(error => {
        const errorCode = error.response.status;
        window.location.replace(`/errors/${errorCode}`);
      });
  }
}

export default Order;
