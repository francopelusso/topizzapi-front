import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faEuroSign, faCalendarDay, faIgloo, faMobileAlt, faUser
} from '@fortawesome/free-solid-svg-icons';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './OrdersItem.css';

class OrdersItem extends Component {
  render() {
    return (
      <div className="OrdersItem">
        <Link to={`/orders/${this.props.orderId}`}>
          <h3 className="OrdersItemData">
            <span className="OrdersItemAt">
              <FontAwesomeIcon icon={faCalendarDay} />
              &nbsp;
              {this.props.orderedAt}
            </span>
          </h3>
        </Link>
        <p>
          &nbsp;
          <span className="OrdersItemName">
            <FontAwesomeIcon icon={faUser} />
            &nbsp;
            {this.props.personName}
          </span>
          <br />
          &nbsp;
          <span className="OrdersItemPrice">
            <FontAwesomeIcon icon={faEuroSign} />
            {this.props.price}
          </span>
          <br />
          <span className="OrdersItemPhone">
            <FontAwesomeIcon icon={faMobileAlt} />
            &nbsp;
            {this.props.phone}
          </span>
          <br />
          <span className="OrdersItemAddress">
            <FontAwesomeIcon icon={faIgloo} />
            &nbsp;
            {this.props.address}
          </span>
        </p>
      </div>
    );
  }
}

export default OrdersItem;
