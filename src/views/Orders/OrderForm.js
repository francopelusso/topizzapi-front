import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faEuroSign, faHotel, faMobileAlt, faPizzaSlice, faUser
} from '@fortawesome/free-solid-svg-icons';
import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';

import './OrderForm.css';
import '../Pizzas/OrderButton.css';

class OrderForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      address: sessionStorage.getItem('orderFormAddress') || '',
      name: sessionStorage.getItem('orderFormName') || '',
      phone: sessionStorage.getItem('orderFormPhone') || '',
      pizza: [],
      pizzaId: this.props.match.params.id,
      success: false,
      errors: null,
      errorClass: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const target = event.target;

    this.setState({ [event.target.name]: target.value });

    const storageName = target.name[0].toUpperCase() + target.name.substring(1);
    sessionStorage.setItem(`orderForm${storageName}`, target.value);
  }

  handleSubmit(event) {
    event.preventDefault();

    const fields = [
      this.state.address,
      this.state.name,
      this.state.phone,
      this.state.pizzaId
    ];
    if (fields.some(field => { return field === ''; })) {
      this.setState({
        errors: 'Please fill all fields.',
        errorClass: 'Errors'
      });
      return;
    }

    axios({
      method: 'post',
      url: `${process.env.REACT_APP_API_URL}/orders/`,
      data: {
        person_name: this.state.name,
        pizza_id: this.state.pizzaId,
        phone_number: this.state.phone,
        address: this.state.address,
        price: 0
      },
      headers: {
        Authorization: `Bearer ${sessionStorage.getItem('TOKEN')}`
      }
    })
      .then(() => {
        this.setState({ success: true });

        for (const field of ['Address', 'Name', 'Phone']) {
          sessionStorage.removeItem(`orderForm${field}`);
        }
      })
      .catch(error => {
        const errorCode = error.response.status;
        window.location.replace(`/errors/${errorCode}`);
      });
  }

  render() {
    if (this.state.success) {
      return (
        <Row className="OrderSuccess">
          <Col xs="12">
            <h1>Pizza ordered successfully.</h1>
          </Col>
          <Col md="4" xs="12">
            <h3 className="Pizza">
              <FontAwesomeIcon icon={faPizzaSlice} />
              &nbsp;
              Pizza
            </h3>
            <div className="PizzaImage">
              <img
                src={this.state.pizza.image_url}
                alt={this.state.pizza.name}
              />
            </div>
            <h5 className="PizzaName">
              {this.state.pizza.name}
            </h5>
          </Col>
          <Col md="2" xs="12">
            <h3 className="OrderLabel">
              <FontAwesomeIcon icon={faUser} />
              &nbsp;
              Name
            </h3>
            <div className="OrderPerson">
              {this.state.name}
            </div>
          </Col>
          <Col md="2" xs="12">
            <h3 className="OrderLabel">
              <FontAwesomeIcon icon={faHotel} />
              Address
            </h3>
            <div className="OrderAddress">
              {this.state.address}
            </div>
          </Col>
          <Col md="2" xs="12">
            <h3 className="OrderLabel">
              <FontAwesomeIcon icon={faMobileAlt} />
              &nbsp;
              Phone
            </h3>
            <div className="OrderPhone">
              {this.state.phone}
            </div>
          </Col>
          <Col md="2" xs="12">
            <h3 className="OrderLabel">
              <FontAwesomeIcon icon={faEuroSign} />
              Price
            </h3>
            <div className="OrderPrice">
              €{this.state.pizza.price}
            </div>
          </Col>
        </Row>
      );
    } else {
      return (
        <Row className="Order">
          <Col md="6" xs="12" className="Pizza">
            <img
              className="PizzaImage"
              src={this.state.pizza.image_url}
              alt={this.state.pizza.name}
            />
            <div className="PizzaName">{this.state.pizza.name}</div>
          </Col>
          <Col md="6" xs="12" className="OrderData">
            <form className="OrderForm" onSubmit={this.handleSubmit}>
              <p className={this.state.errorClass}>
                {this.state.errors}
              </p>
              <label>
                Name:
                &nbsp;
                <input
                  type="text"
                  name="name"
                  onChange={this.handleChange}
                  value={this.state.name}
                />
              </label>
              <br />
              <label>
                Address:
                &nbsp;
                <input
                  type="text"
                  name="address"
                  onChange={this.handleChange}
                  value={this.state.address}
                />
              </label>
              <br />
              <label>
                Phone number:
                &nbsp;
                <input
                  type="text"
                  name="phone"
                  onChange={this.handleChange}
                  value={this.state.phone}
                />
              </label>
              <br />
              <input type="submit" value="Order" className="OrderButton" />
            </form>
          </Col>
        </Row>
      );
    }
  }

  componentDidMount() {
    axios({
      method: 'get',
      url: `
        ${process.env.REACT_APP_API_URL}/pizzas/${this.props.match.params.id}/
      `,
      responseType: 'stream'
    })
      .then(response => {
        this.setState({ pizza: response.data });
      })
      .catch(() => {
        window.location.replace('/');
      });
  }
}

export default OrderForm;
