import axios from 'axios';
import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';

import Pizza from './Pizza';
import PizzasItem from './PizzasItem';

class Pizzas extends Component {
  state = {pizzas: []};

  render() {
    const pizzasItems = this.state.pizzas.map(pizza => {
      return (
        <Col lg="4" md="6" sm="1" key={pizza.id}>
          <PizzasItem
            pizzaId={pizza.id}
            pizzaName={pizza.name}
            imageUrl={pizza.image_url}
            pizzaPrice={pizza.price}
            pizzaScore={pizza.common_score}
          />
        </Col>
      )
    });

    return (
      <div className="PizzasContainer">
        <Row className="PizzasRow">
          {pizzasItems}
        </Row>
      </div>
    );
  }

  componentDidMount() {
    axios({
      method: 'get',
      url: `${process.env.REACT_APP_API_URL}/pizzas/`,
      responseType: 'stream'
    })
      .then(response => {
        this.setState({pizzas: response.data.data.slice(0,10)});
      })
      .catch(error => {
        const errorCode = error.response.status;
        window.location.replace(`/errors/${errorCode}`);
      });
  }
}

export {Pizzas, Pizza};
