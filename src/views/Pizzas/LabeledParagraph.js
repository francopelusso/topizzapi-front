import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { Component } from 'react';

class LabeledParagraph extends Component {
  render() {
    return (
      <p className={this.props.classParagraph}>
        <span className="LabeledLabel">{this.props.label}</span>:
        &nbsp;
        <FontAwesomeIcon icon={this.props.icon} />
        &nbsp;
        <span className="LabeledValue">{this.props.paragraph}</span>
      </p>
    );
  }
}

export default LabeledParagraph;
