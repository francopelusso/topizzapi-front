import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './OrderButton.css';

class OrderButton extends Component {
  render() {
    return (
      <Link to={`/orders/make/${this.props.pizzaId}/`}>
        <div className="OrderButton">Order</div>
      </Link>
    );
  }
}

export default OrderButton;
