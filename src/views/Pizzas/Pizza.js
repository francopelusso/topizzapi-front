import axios from 'axios';
import {
  faUsers, faEuroSign, faCalendarDay
} from '@fortawesome/free-solid-svg-icons';
import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';

import LabeledParagraph from './LabeledParagraph';
import OrderButton from './OrderButton';

import './Pizza.css';

class Pizza extends Component {
  state = {pizza: []};

  render() {
    return (
      <div>
        <h1 className="PizzaName">{this.state.pizza.name}</h1>
        <Row className="PizzaData">
          <Col sm="6" xs="12">
            <img
              className="PizzaImage"
              src={this.state.pizza.image_url}
              alt={this.state.pizza.name}
            />
          </Col>
          <Col className="PizzaDetails" sm="6" xs="12">
            <p className="PizzaDescription">
              {this.state.pizza.description}
            </p>
            <LabeledParagraph
              classParagraph="PizzaPrice"
              label="Price"
              icon={faEuroSign}
              paragraph={this.state.pizza.price}
            />
            <LabeledParagraph
              classParagraph="PizzaScore"
              label="Rate"
              icon={faUsers}
              paragraph={this.state.pizza.common_score}
            />
            <LabeledParagraph
              classParagraph="PizzaUpdated"
              label="Last data update"
              icon={faCalendarDay}
              paragraph={this.state.pizza.updated_at}
            />
            <OrderButton pizzaId={this.state.pizza.id} />
          </Col>
        </Row>
      </div>
    );
  }

  componentDidMount() {
    axios({
      method: 'get',
      url: `
        ${process.env.REACT_APP_API_URL}/pizzas/${this.props.match.params.id}
      `,
      responseType: 'stream'
    })
      .then(response => {
        this.setState({ pizza: response.data });
      })
      .catch(error => {
        const errorCode = error.response.status;
        window.location.replace(`/errors/${errorCode}`);
      });
  }
}

export default Pizza;
