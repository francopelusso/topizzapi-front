import { faUsers, faEuroSign } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import OrderButton from './OrderButton';

import './PizzasItem.css';

class PizzasItem extends Component {
  render() {
    return (
      <div className="PizzasItem">
        <Link to={`/${this.props.pizzaId}/`} className="PizzasItemLink">
          <img
            className="PizzasItemImage"
            src={this.props.imageUrl}
            alt={this.props.pizzaName}
          />
          <h3 className="PizzasItemName">{this.props.pizzaName}</h3>
        </Link>
        <Row className="PizzasItemData">
          <Col md="6" xs="12">
            <div className="PizzasItemPrice">
              <FontAwesomeIcon icon={faEuroSign} />
              &nbsp;
              {this.props.pizzaPrice}
            </div>
          </Col>
          <Col md="6" xs="12">
            <div className="PizzasItemScore">
              <FontAwesomeIcon icon={faUsers} />
              &nbsp;
              {this.props.pizzaScore}
            </div>
          </Col>
          <Col xs="12">
            <OrderButton pizzaId={this.props.pizzaId} />
          </Col>
        </Row>
      </div>
    );
  }
}

export default PizzasItem;
