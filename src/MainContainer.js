import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import ErrorPage from './Errors';
import { LoginForm, Logout, Register } from './views/Users';
import { OrderForm, Orders, Order } from './views/Orders';
import { Pizzas, Pizza } from './views/Pizzas';

class MainContainer extends Component {
  render() {
    return (
      <main className="MainContainer">
        <BrowserRouter basename={process.env.PUBLIC_URL}>
          <Switch>
            <Route exact path="/register/" component={Register} />
            <Route exact path="/login/" component={LoginForm} />
            <Route exact path="/logout/" component={Logout} />
            <Route exact path="/orders/make/:id/" component={OrderForm} />
            <Route exact path="/orders/" component={Orders} />
            <Route exact path="/orders/:id/" component={Order} />
            <Route exact path="/errors/:code/" component={ErrorPage} />
            <Route exact path="/:id/" component={Pizza} />
            <Route exact path="/" component={Pizzas} />
          </Switch>
        </BrowserRouter>
      </main>
    )
  }
}

export default MainContainer;
