# ToPizzApi Front

Simple Reactjs SPA to order one of the 10 most common pizzas.
It should point to [ToPizzApi's REST API](https://gitlab.com/francopelusso/topizzapi).

## Deployment

1. Install nodejs and npm (e.g. `sudo apt-get install nodejs npm`).
1. Clone this repository and move it to **/var/www/html/**.
    ```bash
        $ git clone https://gitlab.com/francopelusso/topizzapi-front
        $ sudo mv topizzapi-front /var/www/html/
    ```
1. Move to **/var/www/html/topizzapi-front** and generate .env file.
    ```bash
        $ cd /var/www/html/topizzapi-front
        $ sudo nano .env
    ```
    File should look like:
    ```bash
        REACT_APP_API_URL=https://topizzapi.tk
        REACT_APP_CLIENT_SECRET=WpXwjlxmbLBP7pZset5cfOmtvI6L9yHPmxFTFG7c
    ```
1. Install dependencies and make a production build:
    ```bash
        $ cd /var/www/html/topizzapi-front
        $ sudo npm install
        $ sudo npm run build
    ```
1. Install nginx (e.g. `sudo apt-get install nginx`).
1. Configure your nginx file similar to this:
    ```nginx
        server {
            root /var/www/html/topizzapi-front/build/;
            server_name topizzapi.tk;
            index index.html index.htm;

            location / {
                try_files $uri /index.html;
            }
        }
    ```
1. You may have multiple nginx servers if the parameter server_name differs.
1. Remember to make a symbolic link from **/etc/nginx/sites-available/my_file** to **/etc/nginx/sites-enabled/my_file**.
    ```bash
        $ sudo ln -s /etc/nginx/sites-available/my_file /etc/nginx/sites-enabled/my_file
    ```
